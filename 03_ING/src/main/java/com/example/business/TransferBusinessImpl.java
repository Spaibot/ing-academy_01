package com.example.business;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.example.repository.TransferRepository;

@Service("TransferBusiness")
public class TransferBusinessImpl implements TransferBusiness {

	@Autowired
	RestTemplate template;
	
	@Autowired
	TransferRepository repository;
	
	String urlBase="http://localhost:9999";
	
	
	@Override
	public boolean checkBic(String bic) {
		boolean check = template.getForObject(urlBase+"/buscar/"+bic, boolean.class);
		return check;
	}


	@Override
	public void addTransfer(TransferEntity transfer) {
		if (checkBic(transfer.getCD_BIC_O()) && checkBic(transfer.getCD_BIC_D())){
			transfer.setDT_DATE(new Date());
			if (transfer.getCD_CURRENCY() == null) {
				transfer.setCD_CURRENCY("EUR");
			}
			repository.addTransfer(transfer);
		}
	}
	
	@Override
	public List<TransferEntity> devolverTransfer() {
		return repository.devolverTransfer();
	}

}
