package com.example.business;

import java.util.List;

public interface TransferBusiness {

	boolean checkBic(String bic);
	void addTransfer (TransferEntity transfer);
	List<TransferEntity> devolverTransfer();
}
