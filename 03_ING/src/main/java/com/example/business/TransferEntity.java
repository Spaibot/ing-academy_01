package com.example.business;

import java.util.Date;

public class TransferEntity {

	private String CD_IBAN_O;
	private String CD_BIC_O;
	private String CD_IBAN_D;
	private String CD_BIC_D;
	private String DS_CONCEPT;
	private String CD_NAME;
	private String CD_CURRENCY;
	private int CD_AMOUNT;
	private Date DT_DATE;
	
	public TransferEntity(String cD_IBAN_O, String cD_BIC_O, String cD_IBAN_D, String cD_BIC_D, String dS_CONCEPT,
			String cD_NAME, String cD_CURRENCY, int cD_AMOUNT, Date dT_DATE) {
		super();
		CD_IBAN_O = cD_IBAN_O;
		CD_BIC_O = cD_BIC_O;
		CD_IBAN_D = cD_IBAN_D;
		CD_BIC_D = cD_BIC_D;
		DS_CONCEPT = dS_CONCEPT;
		CD_NAME = cD_NAME;
		CD_CURRENCY = cD_CURRENCY;
		CD_AMOUNT = cD_AMOUNT;
		DT_DATE = dT_DATE;
	}

	public TransferEntity() {
		super();
	}

	public String getCD_IBAN_O() {
		return CD_IBAN_O;
	}

	public void setCD_IBAN_O(String cD_IBAN_O) {
		CD_IBAN_O = cD_IBAN_O;
	}

	public String getCD_BIC_O() {
		return CD_BIC_O;
	}

	public void setCD_BIC_O(String cD_BIC_O) {
		CD_BIC_O = cD_BIC_O;
	}

	public String getCD_IBAN_D() {
		return CD_IBAN_D;
	}

	public void setCD_IBAN_D(String cD_IBAN_D) {
		CD_IBAN_D = cD_IBAN_D;
	}

	public String getCD_BIC_D() {
		return CD_BIC_D;
	}

	public void setCD_BIC_D(String cD_BIC_D) {
		CD_BIC_D = cD_BIC_D;
	}

	public String getDS_CONCEPT() {
		return DS_CONCEPT;
	}

	public void setDS_CONCEPT(String dS_CONCEPT) {
		DS_CONCEPT = dS_CONCEPT;
	}

	public String getCD_NAME() {
		return CD_NAME;
	}

	public void setCD_NAME(String cD_NAME) {
		CD_NAME = cD_NAME;
	}

	public String getCD_CURRENCY() {
		return CD_CURRENCY;
	}

	public void setCD_CURRENCY(String cD_CURRENCY) {
		CD_CURRENCY = cD_CURRENCY;
	}

	public int getCD_AMOUNT() {
		return CD_AMOUNT;
	}

	public void setCD_AMOUNT(int cD_AMOUNT) {
		CD_AMOUNT = cD_AMOUNT;
	}

	public Date getDT_DATE() {
		return DT_DATE;
	}

	public void setDT_DATE(Date dT_DATE) {
		DT_DATE = dT_DATE;
	}
}