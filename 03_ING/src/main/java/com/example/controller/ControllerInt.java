package com.example.controller;

import java.util.List;

import com.example.business.TransferEntity;

public interface ControllerInt {

	void addTransfer(TransferEntity transfer);
	List<TransferEntity> devolverTransfer();

}