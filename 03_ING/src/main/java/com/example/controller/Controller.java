package com.example.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.business.TransferBusiness;
import com.example.business.TransferEntity;

@CrossOrigin(origins="*")
@RestController
public class Controller implements ControllerInt {

	@Autowired
	TransferBusiness business;
	
	@Override
	@PostMapping(value="transfer",consumes=MediaType.APPLICATION_JSON_VALUE)
	public void addTransfer(@RequestBody TransferEntity transfer) {
		business.addTransfer(transfer);
	}
	
	@Override
	@GetMapping(value="bics",produces=MediaType.APPLICATION_JSON_VALUE)
	public List<TransferEntity> devolverTransfer() {
		return business.devolverTransfer();
	}
	
}
