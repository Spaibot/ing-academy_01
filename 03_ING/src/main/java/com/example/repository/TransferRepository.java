package com.example.repository;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.example.business.TransferEntity;

@Mapper
public interface TransferRepository {

	
	@Insert("insert into TRANSFER (CD_IBAN_O,"
								+ "CD_BIC_O,"
								+ "CD_IBAN_D,"
								+ "CD_BIC_D,"
								+ "CD_AMOUNT,"
								+ "CD_NAME,"
								+ "CD_CURRENCY,"
								+ "DS_CONCEPT,"
								+ "DT_DATE) "
								+ "VALUES ( #{transfer.CD_IBAN_O},"
										 + "#{transfer.CD_BIC_O},"
										 + "#{transfer.CD_IBAN_D},"
										 + "#{transfer.CD_BIC_D},"
										 + "#{transfer.CD_AMOUNT},"
										 + "#{transfer.CD_NAME},"
										 + "#{transfer.CD_CURRENCY},"
										 + "#{transfer.DS_CONCEPT},"
										 + "#{transfer.DT_DATE})")
	void addTransfer(@Param("transfer")TransferEntity transfer);
	
	@Select("select * from TRANSFER")
	List<TransferEntity> devolverTransfer();
	
}
