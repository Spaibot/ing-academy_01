package com.example.dao;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.example.model.REGIONS;

@Mapper
public interface RegionDao {

	@Select("select * from REGIONS")
	List<REGIONS> devolverRegiones();
	
	@Insert("insert into REGIONS (REGION_ID, REGION_NAME) VALUES ( #{region.REGION_ID}, #{region.REGION_NAME})")
	void agregarRegion(@Param("region")REGIONS region);
	
}
