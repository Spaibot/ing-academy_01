package com.example.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.dao.RegionDao;
import com.example.model.REGIONS;

@Service("ServicioRegion")
public class RegionServiceImpl implements RegionService {

	@Autowired
	RegionDao regiones;
	
	@Override
	public List<REGIONS> devolverRegiones() {
		return regiones.devolverRegiones();
	}

	@Override
	public void agregarRegion(REGIONS region) {
		regiones.agregarRegion(region);
	}

}
