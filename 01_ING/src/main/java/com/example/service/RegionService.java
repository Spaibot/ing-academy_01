package com.example.service;

import java.util.List;

import com.example.model.REGIONS;

public interface RegionService {

	
	List<REGIONS> devolverRegiones();
	void agregarRegion(REGIONS region);
	
}
