package com.example.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.example.model.REGIONS;
import com.example.service.RegionService;

@CrossOrigin(origins="*")
@RestController
public class Controller {

	@Autowired
	RegionService service;
	
	@GetMapping(value="regiones",produces=MediaType.APPLICATION_JSON_VALUE)
	public List<REGIONS> listaRegiones() {
		return service.devolverRegiones();
	}
	
	@GetMapping(value="generar/{id}/{nombre}",produces=MediaType.APPLICATION_JSON_VALUE)
	public void generarRegion(@PathVariable("nombre") String nombre,
							  @PathVariable("id") int id) {
		service.agregarRegion(new REGIONS(id,nombre));
	}
	
	
}
