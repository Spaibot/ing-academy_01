package com.example.repository;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.example.business.BicEntity;

@Mapper
public interface BicRepository {

	@Select("select * from BIC")
	List<BicEntity> devolverBics();
	
	@Insert("insert into BIC (CD_ENTIDAD, CD_PAIS, CD_LOCALIDAD, CD_OFICINA, B_ACTIVO) VALUES ( #{bic.CD_ENTIDAD}, #{bic.CD_PAIS}, #{bic.CD_LOCALIDAD}, #{bic.CD_OFICINA}, #{bic.B_ACTIVO})")
	void agregarBic(@Param("bic")BicEntity bic);
	
	@Update("update BIC set B_ACTIVO = #{bic.B_ACTIVO} WHERE (CD_ENTIDAD = #{bic.CD_ENTIDAD} AND CD_PAIS = #{bic.CD_PAIS} AND CD_LOCALIDAD = #{bic.CD_LOCALIDAD} AND CD_OFICINA = #{bic.CD_OFICINA})")
	void actualizarEstado(@Param("bic")BicEntity bic);
	
}
