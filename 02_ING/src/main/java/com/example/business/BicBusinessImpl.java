package com.example.business;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.repository.BicRepository;

@Service("BicBusiness")
public class BicBusinessImpl implements BicBusiness {

	@Autowired
	BicRepository repository;
	
	@Override
	public List<BicEntity> devolverBics() {
		return repository.devolverBics();
	}

	@Override
	public void agregarBic(BicEntity bic) {
		if (bic.getCD_OFICINA()==null) {
			bic.setCD_OFICINA("XXX");
		}
		repository.agregarBic(bic);
	}

	@Override
	public void actualizarEstado(BicEntity bic) {
		repository.actualizarEstado(bic);
	}

	@Override
	public boolean checkBic(String bic) {
		return repository.devolverBics().stream().anyMatch(a-> a.getBic().equals(bic));
	}

}
