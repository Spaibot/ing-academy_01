package com.example.business;

import java.util.List;

public interface BicBusiness {

	List<BicEntity> devolverBics();
	void agregarBic(BicEntity bic);
	void actualizarEstado(BicEntity bic);
	boolean checkBic(String bic);
}
