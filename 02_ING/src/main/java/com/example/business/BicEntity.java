package com.example.business;

public class BicEntity {

	private String CD_ENTIDAD;
	private String CD_PAIS;
	private String CD_LOCALIDAD;
	private String CD_OFICINA;
	private int B_ACTIVO;
	
	
	public BicEntity(String cD_ENTIDAD, String cD_PAIS, String cD_LOCALIDAD, String cD_OFICINA, int b_ACTIVO) {
		super();
		CD_ENTIDAD = cD_ENTIDAD;
		CD_PAIS = cD_PAIS;
		CD_LOCALIDAD = cD_LOCALIDAD;
		CD_OFICINA = cD_OFICINA;
		B_ACTIVO = b_ACTIVO;
	}

	public BicEntity() {
		super();
	}

	public String getCD_ENTIDAD() {
		return CD_ENTIDAD;
	}

	public void setCD_ENTIDAD(String cD_ENTIDAD) {
		CD_ENTIDAD = cD_ENTIDAD;
	}

	public String getCD_PAIS() {
		return CD_PAIS;
	}

	public void setCD_PAIS(String cD_PAIS) {
		CD_PAIS = cD_PAIS;
	}

	public String getCD_LOCALIDAD() {
		return CD_LOCALIDAD;
	}

	public void setCD_LOCALIDAD(String cD_LOCALIDAD) {
		CD_LOCALIDAD = cD_LOCALIDAD;
	}

	public String getCD_OFICINA() {
		return CD_OFICINA;
	}

	public void setCD_OFICINA(String cD_OFICINA) {
		CD_OFICINA = cD_OFICINA;
	}

	public int getB_ACTIVO() {
		return B_ACTIVO;
	}

	public void setB_ACTIVO(int b_ACTIVO) {
		B_ACTIVO = b_ACTIVO;
	}

	public String getBic() {
		return CD_ENTIDAD+CD_PAIS+CD_LOCALIDAD+CD_OFICINA;
	}
	
	
	@Override
	public String toString() {
		return "BicEntity [CD_ENTIDAD=" + CD_ENTIDAD + ", CD_PAIS=" + CD_PAIS + ", CD_LOCALIDAD=" + CD_LOCALIDAD
				+ ", CD_OFICINA=" + CD_OFICINA + ", B_ACTIVO=" + B_ACTIVO + "]";
	}
	
	
	
	
}