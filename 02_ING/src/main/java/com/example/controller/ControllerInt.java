package com.example.controller;

import java.util.List;

import com.example.business.BicEntity;

public interface ControllerInt {

	List<BicEntity> listaBics();
	void agregarBic(BicEntity bic);
	void actualizarBic(BicEntity bic);
	boolean checkBic(String bic);

}