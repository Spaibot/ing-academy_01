package com.example.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.business.BicBusiness;
import com.example.business.BicEntity;

@CrossOrigin(origins="*")
@RestController
public class Controller implements ControllerInt {

	
	@Autowired
	BicBusiness business;
	
	@Override
	@GetMapping(value="bics",produces=MediaType.APPLICATION_JSON_VALUE)
	public List<BicEntity> listaBics() {
		return business.devolverBics();
	}
	
	@Override
	@PostMapping(value="bics",consumes=MediaType.APPLICATION_JSON_VALUE)
	public void agregarBic(@RequestBody BicEntity bic) {
		business.agregarBic(bic);
	}
	
	@Override
	@PutMapping(value="bics",consumes=MediaType.APPLICATION_JSON_VALUE)
	public void actualizarBic(@RequestBody BicEntity bic) {
		business.actualizarEstado(bic);
	}
	
	@Override
	@GetMapping(value="buscar/{bic}",produces=MediaType.APPLICATION_JSON_VALUE)
	public boolean checkBic(@PathVariable("bic") String bic) {
		return business.checkBic(bic);
	}
}
